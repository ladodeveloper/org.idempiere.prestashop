/**
 * 
 */
package org.idempiere.eshop.wsclient.synch;


import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.transform.TransformerException;


import org.compiere.model.MMZI_ES_Synchronizations;
import org.compiere.model.MProduct;
import org.compiere.model.MStorageOnHand;
import org.compiere.model.MStore;
import org.compiere.model.Query;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.idempiere.eshop.model.X_MZI_EShop_Mapping;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

/**
 * @author Edgar Gonzalez
 *
 */
public class SynchStock extends EShopInitialization {

	private MStore store=null;
	private Map<Integer ,Integer > mapProducts = new HashMap<Integer ,Integer >();
	private Map<Integer ,Integer > mapStorages = new HashMap<Integer ,Integer >();
	private PSWebServiceClient ws;	
	private Timestamp lastsuccess;
	
	/**
	 * 
	 */
	public SynchStock(MStore store) {
		
		super(store);
		
		this.store=store;
	}
	
	public void run() {
		MMZI_ES_Synchronizations synch = MMZI_ES_Synchronizations.getLastSuccess(Env.getCtx(),
				MMZI_ES_Synchronizations.SYNCHRONIZATIONTYPE_StockCurrent, MMZI_ES_Synchronizations.EXTERNALSYSTEM_PrestaShop , store.getW_Store_ID(), 0, null);

		if (this.ws==null)
		    this.ws = new PSWebServiceClient(store.getURL(),store.getWStoreUserPW(),false);
		
		mapProducts=getMapProducts();
		mapStorages=getMapStorages();
		long start = Calendar.getInstance().getTimeInMillis();
		
		List<MProduct> products=getStoragesToSynch( synch ,store.getC_Vocabulary_ID());
		HashMap<String,Object> getSchemaOpt = new HashMap();

		try {
			getSchemaOpt.put("url", store.getURL()+"/api/stock_availables?schema=blank");       
			Document schema = ws.get(getSchemaOpt);
		
		
		for (MProduct product:products){
			System.out.println("Product Stock available started "+product.getValue());
		
			if (mapProducts.get(product.get_ID())==null)
					continue;
			
			
			 HashMap<String,Object> productOpt = new HashMap();
		        productOpt.put("resource", "stock_availables");

			if (!mapStorages.containsKey(product.get_ID()))
			{
				//productOpt.put("url", store.getURL()+"/api/stock_availables/");
				HashMap<String,Object> productget = new HashMap();
				productget.put("resource", "stock_availables");
				productget.put("filter", "[id_product]="+ mapProducts.get(product.get_ID())+"");
				
				Document ws_storage = ws.get(productget);    
				System.out.println(ws.DocumentToString(ws_storage));
				if (ws_storage.getElementsByTagName("stock_available").getLength()>0){
			        Node storage= ws_storage.getElementsByTagName("stock_available").item(0);
			        NamedNodeMap attrs = storage.getAttributes();
		            String storage_id = attrs.getNamedItem("id").getNodeValue();
		           	updateStorageMap(product, Integer.parseInt(storage_id));
		        
		        } else continue;    
	
			
			}

			schema.getElementsByTagName("id").item(0).setTextContent(String.valueOf(mapStorages.get(product.get_ID())));  
	        schema.getElementsByTagName("id_product").item(0).setTextContent(String.valueOf(mapProducts.get(product.get_ID()))); 
	        schema.getElementsByTagName("quantity").item(0).setTextContent(product.getStorage().setScale(0, RoundingMode.HALF_UP).toString());
	        schema.getElementsByTagName("id_product_attribute").item(0).setTextContent("0");
	        schema.getElementsByTagName("depends_on_stock").item(0).setTextContent("0");
	        schema.getElementsByTagName("out_of_stock").item(0).setTextContent("1");
	        schema.getElementsByTagName("id_shop").item(0).setTextContent("1");
			productOpt.put("url", store.getURL()+"/api/stock_availables/"+mapStorages.get(product.get_ID()));
			
			productOpt.put("id", mapStorages.get(product.get_ID()));
			productOpt.put("putXml", ws.DocumentToString(schema));
	        Document ws_storage = ws.edit(productOpt);     
	        System.out.println(ws.DocumentToString(ws_storage));
		}
		
		long end = Calendar.getInstance().getTimeInMillis();
		System.out.println("Products Stock avalilable executed");
		
		MMZI_ES_Synchronizations synchsucces=new MMZI_ES_Synchronizations(Env.getCtx(), 0, null);
		synchsucces.setAD_Org_ID(store.getAD_Org_ID());
		synchsucces.setSuccess(true);
		synchsucces.setExternalSystem("PS");
		synchsucces.setTimestampLocal(new Timestamp(start));
		synchsucces.setSynchronizationType(MMZI_ES_Synchronizations.SYNCHRONIZATIONTYPE_StockCurrent);
		synchsucces.setW_Store_ID(store.get_ID());
		synchsucces.saveEx();
		setMapStorages(mapStorages);
		
		} catch (PrestaShopWebserviceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}     
	}
	
	public void initialize() {
			DB.executeUpdate("DELETE FROM MZI_ES_Synchronizations WHERE SYNCHRONIZATIONTYPE='"+ MMZI_ES_Synchronizations.SYNCHRONIZATIONTYPE_StockCurrent+"' AND W_Store_ID="+store.get_ID(),null);
	}

	
	public void initialize2() {
		DB.executeUpdate("DELETE FROM MZI_EShop_Mapping WHERE AD_Table_ID="+MProduct.Table_ID+" AND W_Store_ID="+store.get_ID(),null);
		DB.executeUpdate("DELETE FROM MZI_ES_Synchronizations WHERE SYNCHRONIZATIONTYPE='"+ MMZI_ES_Synchronizations.SYNCHRONIZATIONTYPE_ProductImage+"' AND W_Store_ID="+store.get_ID(),null);
	}
	
	private List<MProduct> getStoragesToSynch( MMZI_ES_Synchronizations synch,int  C_Vocabulary_ID) {
		
		if ( synch==null)
			lastsuccess= new Timestamp(0);
		else 
			lastsuccess=synch.getTimestampLocal();	
		
		List<MProduct> products = new Query(Env.getCtx(), MProduct.Table_Name,
				" m_product_id in (select m_product_id from m_transaction im where im.ad_client_id=? and im.created>=?) "
						+ " AND M_Product_ID in ( select ci.record_id from  C_ClassificationInstance ci join c_classification cl on cl.c_classification_id=ci.c_classification_id " 
						+ " where cl.C_Vocabulary_ID=?) ",
				null)
				.setParameters(Env.getAD_Client_ID(Env.getCtx()), lastsuccess,C_Vocabulary_ID)
				.list();
	
		return products;
	}
	
	
	
	public void updateStorageMap(MProduct product , int storage_id){

		 if (!mapStorages.containsKey(product.get_ID())&& storage_id>0 ){
				X_MZI_EShop_Mapping mapping= new X_MZI_EShop_Mapping(Env.getCtx(),0,null);
				mapping.setAD_Table_ID(MStorageOnHand.Table_ID);
				mapping.setEshop_Ref_ID(storage_id );
				mapping.setRecord_ID(product.get_ID());
				mapping.setExternalSystem(X_MZI_EShop_Mapping.EXTERNALSYSTEM_PrestaShop);
				mapping.setW_Store_ID( store.get_ID());
				mapping.saveEx();
				mapStorages.put(product.get_ID(), storage_id);
			 
		 }
		 
	 
    }

	
	
}
