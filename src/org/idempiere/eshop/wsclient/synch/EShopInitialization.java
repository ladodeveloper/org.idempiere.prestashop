package org.idempiere.eshop.wsclient.synch;

import static java.lang.Math.toIntExact;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;

import com.edgaragg.pshop4j.PrestaShopWebservice;
import com.edgaragg.pshop4j.model.Filter;
import com.edgaragg.pshop4j.modeling.PrestaShopAsyncMapper;
import com.edgaragg.pshop4j.modeling.PrestaShopMapper;

import org.compiere.model.MBPartner;
import org.compiere.model.MImage;
import org.compiere.model.MOrder;
import org.compiere.model.MProduct;
import org.compiere.model.MProductCategory;
import org.compiere.model.MStorageOnHand;
import org.compiere.model.MStore;
import org.compiere.util.CPreparedStatement;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.idempiere.eshop.model.X_MZI_EShop_Mapping; 

public class EShopInitialization {

	private PrestaShopWebservice web;
	private PrestaShopMapper mapper;
	private PrestaShopAsyncMapper asyncMapper;
	private List<Filter> filters = Collections.emptyList();
	private Map<Integer ,Integer > mapCategories = new HashMap<Integer ,Integer >();
	private Map<Integer ,Integer > mapProducts = new HashMap<Integer ,Integer >();
	private Map<Integer ,Integer > mapImages = new HashMap<Integer ,Integer >();
	private Map<Integer ,Integer > mapStorages = new HashMap<Integer ,Integer >();
	private Map<Integer ,Integer > mapCustomers = new HashMap<Integer ,Integer >();
	private Map<Integer ,Integer > mapLocations = new HashMap<Integer ,Integer >();
	private Map<Integer ,Integer > mapOrders = new HashMap<Integer ,Integer >();



	private int w_store_id;
	
	public EShopInitialization(MStore store) {

		web = new PrestaShopWebservice(store.getURL(), store.getWStoreUserPW());
		mapper = new PrestaShopMapper(web);
		asyncMapper = new PrestaShopAsyncMapper(web);
		w_store_id = store.get_ID();
	 
	}


	/**
	 * @return the web
	 */
	public PrestaShopWebservice getWeb() {
		return web;
	}


	/**
	 * @param web the web to set
	 */
	public void setWeb(PrestaShopWebservice web) {
		this.web = web;
	}


	/**
	 * @return the mapper
	 */
	public PrestaShopMapper getMapper() {
		return mapper;
	}


	/**
	 * @param mapper the mapper to set
	 */
	public void setMapper(PrestaShopMapper mapper) {
		this.mapper = mapper;
	}


	/**
	 * @return the filters
	 */
	public List<Filter> getFilters() {
		return filters;
	}


	/**
	 * @param filters the filters to set
	 */
	public void setFilters(List<Filter> filters) {
		this.filters = filters;
	}


	/**
	 * @return the asyncMapper
	 */
	public PrestaShopAsyncMapper getAsyncMapper() {
		return asyncMapper;
	}


	/**
	 * @param asyncMapper the asyncMapper to set
	 */
	public void setAsyncMapper(PrestaShopAsyncMapper asyncMapper) {
		this.asyncMapper = asyncMapper;
	}
	
	
	public Map<Integer, Integer> getMapCategories() {
		if (mapCategories.isEmpty())
			mapCategories();
		return mapCategories;
	}
	
	public void setMapCategories(Map<Integer, Integer> mapCategories) {
		this.mapCategories = mapCategories;
	}
	
	private void mapCategories() {
		mapCategories.clear();
		CPreparedStatement pstmt = null;
		ResultSet rs=null;
		try{
			pstmt = DB.prepareStatement ("SELECT Record_id, Eshop_Ref_ID FROM MZI_EShop_Mapping WHERE  AD_Table_ID="+MProductCategory.Table_ID+" AND W_Store_ID="+w_store_id, null);
			rs = pstmt.executeQuery ();
			while ( rs.next() ) mapCategories.put( rs.getInt(1), rs.getInt(2) );
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		finally{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		};	System.out.println(" Mapping Categories Done!");
	}

	public long getCategorieDefault(MProduct product,MStore store){
		int classification_id=DB.getSQLValue(null,"SELECT  ci.C_Classification_ID FROM  C_ClassificationInstance  ci join c_classification cl on cl.c_classification_id=ci.c_classification_id"
				+ " WHERE record_id="+product.get_ID()+" AND cl.c_vocabulary_id="+store.getC_Vocabulary_ID());
		return getMapCategories().get(classification_id);
	}


	public Map<Integer ,Integer > getMapProducts() {
		if (mapProducts.isEmpty())
			mapProducts();
		return mapProducts;
	}


	public void setMapProducts(Map<Integer ,Integer > mapProducts) {
		this.mapProducts = mapProducts;
	}
	
	
	private void mapProducts() {
		
		mapProducts.clear();
		CPreparedStatement pstmt = null;
		ResultSet rs=null;
		try{
			pstmt = DB.prepareStatement ("SELECT Record_id, Eshop_Ref_ID FROM MZI_EShop_Mapping WHERE  AD_Table_ID="+MProduct.Table_ID+" AND W_Store_ID="+w_store_id, null);
			rs = pstmt.executeQuery ();
			while ( rs.next() ) mapProducts.put( rs.getInt(1), rs.getInt(2) );
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		finally{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		};	System.out.println(" Mapping Products Done!");
	}
	

	public Map<Integer ,Integer > getMapOrders(Boolean requery ) {
		if (mapOrders.isEmpty()||requery)
			mapOrders();
		return mapOrders;
	}


	public void setMapOrders(Map<Integer ,Integer > mapOrders) {
		this.mapOrders = mapOrders;
	}


	private void mapOrders() {
		
		mapOrders.clear();
		CPreparedStatement pstmt = null;
		ResultSet rs=null;
		try{
			pstmt = DB.prepareStatement ("SELECT Record_id, Eshop_Ref_ID FROM MZI_EShop_Mapping WHERE  AD_Table_ID="+MOrder.Table_ID+" AND W_Store_ID="+w_store_id, null);
			rs = pstmt.executeQuery ();
			while ( rs.next() ) mapOrders.put( rs.getInt(1), rs.getInt(2) );
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		finally{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		};	System.out.println(" Mapping Products Done!");
	}

	public Map<Integer ,Integer > getMapImages() {
		if (mapImages.isEmpty())
			mapImages();
		return mapImages;
	}


	public void setMapImages(Map<Integer ,Integer > mapImages) {
		this.mapImages = mapImages;
	}
	
	private void mapImages() {
		
		mapImages.clear();
		CPreparedStatement pstmt = null;
		ResultSet rs=null;
		try{
			pstmt = DB.prepareStatement ("SELECT Record_id, Eshop_Ref_ID FROM MZI_EShop_Mapping WHERE  AD_Table_ID="+MImage.Table_ID+" AND W_Store_ID="+w_store_id, null);
			rs = pstmt.executeQuery ();
			while ( rs.next() ) mapImages.put( rs.getInt(1), rs.getInt(2) );
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		finally{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		};	System.out.println(" Mapping Images Done!");
	}


	private void mapStorages() {
		
		mapStorages.clear();
		CPreparedStatement pstmt = null;
		ResultSet rs=null;
		try{
			pstmt = DB.prepareStatement ("SELECT Record_id, Eshop_Ref_ID FROM MZI_EShop_Mapping WHERE  AD_Table_ID="+MStorageOnHand.Table_ID+" AND W_Store_ID="+w_store_id, null);
			rs = pstmt.executeQuery ();
			while ( rs.next() ) mapStorages.put( rs.getInt(1), rs.getInt(2) );
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		finally{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		};	System.out.println(" Mapping Storages Done!");
	}

	public Map<Integer ,Integer > getMapStorages() {
		if (mapStorages.isEmpty())
			mapStorages();
		return mapStorages;
	}


	public void setMapStorages(Map<Integer ,Integer > mapStorages) {
		this.mapStorages = mapStorages;
	}
	
	public Map<Integer ,Integer > getMapCustomers(Boolean requery ) {
		if (mapCustomers.isEmpty()||requery)
			mapCustomers();
		return mapCustomers;
	}


	public void setMapCustomers(Map<Integer ,Integer > mapCustomers) {
		this.mapCustomers = mapCustomers;
	}


	private void mapCustomers() {
		
		mapCustomers.clear();
		CPreparedStatement pstmt = null;
		ResultSet rs=null;
		try{
			pstmt = DB.prepareStatement ("SELECT Record_id, Eshop_Ref_ID FROM MZI_EShop_Mapping WHERE  AD_Table_ID="+MBPartner.Table_ID+" AND W_Store_ID="+w_store_id, null);
			rs = pstmt.executeQuery ();
			while ( rs.next() ) mapCustomers.put( rs.getInt(1), rs.getInt(2) );
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		finally{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		};	System.out.println(" Mapping Products Done!");
	}
	
	public Map<Integer ,Integer > getMapLocations(Boolean requery ) {
		if (mapLocations.isEmpty()||requery)
			mapLocations();
		return mapLocations;
	}


	public void setMapLocations(Map<Integer ,Integer > mapLocations) {
		this.mapLocations = mapLocations;
	}


	private void mapLocations() {
		
		mapLocations.clear();
		CPreparedStatement pstmt = null;
		ResultSet rs=null;
		try{
			pstmt = DB.prepareStatement ("SELECT Record_id, Eshop_Ref_ID FROM MZI_EShop_Mapping WHERE  AD_Table_ID="+MBPartner.Table_ID+" AND W_Store_ID="+w_store_id, null);
			rs = pstmt.executeQuery ();
			while ( rs.next() ) mapLocations.put( rs.getInt(1), rs.getInt(2) );
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		finally{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		};	System.out.println(" Mapping Products Done!");
	}


	public static Integer getKeyByValue(Map<Integer, Integer> map, Integer value) {
	    for (Entry<Integer, Integer> entry : map.entrySet()) {
	        if (Objects.equals(value, entry.getValue())) {
	            return entry.getKey();
	        }
	    }
	    return 0;
	}
	
	public static void addMapping(Map<Integer, Integer> map, int store_ID, int table_ID, int eshop_id, int system_id, String trx_name) {
		// add to mapping table
		X_MZI_EShop_Mapping mapping= new X_MZI_EShop_Mapping(Env.getCtx(),0,trx_name);
		mapping.setAD_Table_ID(table_ID);
		mapping.setEshop_Ref_ID(eshop_id );
		mapping.setRecord_ID(system_id);
		mapping.setExternalSystem(X_MZI_EShop_Mapping.EXTERNALSYSTEM_PrestaShop);
		mapping.setW_Store_ID(store_ID);
		mapping.saveEx();
		map.put(system_id, eshop_id);
	}
}
