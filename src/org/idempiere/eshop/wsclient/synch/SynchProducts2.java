/**
 * 
 */
package org.idempiere.eshop.wsclient.synch;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.transform.TransformerException;
import static java.lang.Math.toIntExact;

import org.compiere.model.MClassification;
import org.compiere.model.MMZI_ES_Synchronizations;
import org.compiere.model.MProduct;
import org.compiere.model.MProductPrice;
import org.compiere.model.MStorageOnHand;
import org.compiere.model.MStore;
import org.compiere.model.MTax;
import org.compiere.model.MTaxCategory;
import org.compiere.model.Query;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.idempiere.eshop.model.X_MZI_EShop_Mapping;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.edgaragg.pshop4j.model.Limit;
import com.edgaragg.pshop4j.model.Sort;
import com.edgaragg.pshop4j.modeling.PrestaShopMapperResponse;
import com.edgaragg.pshop4j.pojos.PrestaShopPojoEntity;
import com.edgaragg.pshop4j.pojos.entities.Product;
import com.edgaragg.pshop4j.pojos.list.Products;
import org.apache.commons.lang.StringEscapeUtils;
/**
 * @author Edgar Gonzalez
 *
 */
public class SynchProducts2 extends EShopInitialization {

	private MStore store=null;
	private Map<Integer ,Integer > mapProducts = new HashMap<Integer ,Integer >();
	private Map<Integer ,Integer > mapStorages = new HashMap<Integer ,Integer >();
	private Timestamp lastsuccess;
	
	/**
	 * 
	 */
	public SynchProducts2(MStore store) {
		
		super(store);
		
		this.store=store;
	}
	
	public String run() {
		MMZI_ES_Synchronizations synch = MMZI_ES_Synchronizations.getLastSuccess(Env.getCtx(),
				MMZI_ES_Synchronizations.SYNCHRONIZATIONTYPE_Product, MMZI_ES_Synchronizations.EXTERNALSYSTEM_PrestaShop , store.getW_Store_ID(), 0, null);

		
		String errorproducts="";
		mapProducts=getMapProducts();
		mapStorages=getMapStorages();
		
		long start = Calendar.getInstance().getTimeInMillis();
		
		List<MProduct> products=getProductsToSynch( synch ,store.getC_Vocabulary_ID());
		
		
		
		for (MProduct product:products){
			System.out.println("Product started "+product.getValue());
			Product entity = new Product();

			try {
				
			HashMap<String,Object> getSchemaOpt = new HashMap();
			MProductPrice pp=MProductPrice.getLatest(Env.getCtx(),store.getM_PriceList_ID(), product.getM_Product_ID(), null);
			if (pp==null||pp.getM_PriceList_Version()==null) {
				errorproducts=errorproducts+"\n Product:"+product.getValue()+" missing pricelist";
				continue;
			}
			BigDecimal price=Env.ZERO;
			if (pp.getM_PriceList_Version().getM_PriceList().isTaxIncluded())
			{
				MTax stdTax = new MTax (Env.getCtx(), 
						((MTaxCategory) product.getC_TaxCategory()).getDefaultTax().getC_Tax_ID(), 
						null);
				BigDecimal taxStdAmt = stdTax.calculateTax(pp.getPriceList(), true, 2);
				price=pp.getPriceList().subtract(taxStdAmt);
			}
			else
			price=pp.getPriceList();
			
			
			if (this.ws==null)
			    this.ws = new PSWebServiceClient(store.getURL(),store.getWStoreUserPW(),false);
	
			getSchemaOpt.put("url", store.getURL()+"/api/products?schema=blank");       
			Document schema = ws.get(getSchemaOpt);     
	        schema.getElementsByTagName("id_category_default").item(0).setTextContent(String.valueOf(getCategorieDefault(product,store)));
	        if (getMapImages().get(product.get_ID())!=null){
	        	schema.getElementsByTagName("id_default_image").item(0).setTextContent(String.valueOf(getMapImages().get(product.get_ID())));
	        	Element id_default_image = (Element) schema.getElementsByTagName("id_default_image").item(0);
	        	id_default_image.setAttribute("xlink:href",store.getURL()+"/images/products/"+mapProducts.get(product.get_ID()+"/"+getMapImages().get(product.get_ID()))) ;        
	        }
	        schema.getElementsByTagName("price").item(0).setTextContent(price.toString()); 
	        schema.getElementsByTagName("available_for_order").item(0).setTextContent("1");
	        {
	        schema.getElementsByTagName("quantity").item(0).setNodeValue(product.getStorage().toString()); //toto nieje stav na sklad ale hladaj stock_available
	        
//		        Element stock_availables = (Element) schema.getElementsByTagName("stock_availables").item(0).getFirstChild();
//		        Element stock_available= (Element) schema.getElementsByTagName("stock_availables").item(0).getFirstChild();
//		        stock_availables.appendChild(stock_available);
//		        stock_availables.appendChild(schema.createCDATASection(StringEscapeUtils.escapeXml(product.getHelp())));
//		        stock_availables.setAttribute("nodeType", "stock_available");
//		        stock_availables.setAttribute("api", "stock_availables");
	        }
	        schema.getElementsByTagName("show_price").item(0).setTextContent("1"); 
	        schema.getElementsByTagName("indexed").item(0).setTextContent("1");
	        schema.getElementsByTagName("id_tax_rules_group").item(0).setTextContent("1");  
			if (!product.isActive())
				schema.getElementsByTagName("active").item(0).setTextContent("0");  //True = 1  False = 2 
			else 
		        schema.getElementsByTagName("active").item(0).setTextContent("1"); 

			List<MClassification> categories=getCategoriesByProduct(product.get_ID(),store.getC_Vocabulary_ID());
			
			for (MClassification cat:categories){
			
				Element category = schema.createElement("category");
		        Element catId = schema.createElement("id");
		        catId.setTextContent(String.valueOf(getMapCategories().get(cat.getC_Classification_ID())));
		        category.appendChild(catId);
		        schema.getElementsByTagName("categories").item(0).appendChild(category);
		       
			}
	        
	        Element link_rewrite = (Element) schema.getElementsByTagName("link_rewrite").item(0).getFirstChild();
	        link_rewrite.appendChild(schema.createCDATASection(product.getName().toLowerCase().replaceAll("[^A-Za-z0-9\\s]", "").replace(" ","-")));
	        link_rewrite.setAttribute("id", "2");
	        link_rewrite.setAttribute("xlink:href",store.getURL()+"/api/languages/"+2);    
	        
	        if (product.getName()!=null){
		        Element meta_title = (Element) schema.getElementsByTagName("meta_title").item(0).getFirstChild();
		        meta_title.appendChild(schema.createCDATASection(StringEscapeUtils.escapeXml(product.getName())));
		        meta_title.setAttribute("id", "2");
		        meta_title.setAttribute("xlink:href",store.getURL()+"/api/languages/"+2);    

		        Element name = (Element) schema.getElementsByTagName("name").item(0).getFirstChild();
		        name.appendChild(schema.createCDATASection(product.getName()));
		        name.setAttribute("id", "2");
		        name.setAttribute("xlink:href", store.getURL() +"/api/languages/"+2);

	        }
	        
	        
	        if (product.getDescription()!=null){
		        Element description = (Element) schema.getElementsByTagName("description_short").item(0).getFirstChild();
		        description.appendChild(schema.createCDATASection(product.getDescription()));
		        description.setAttribute("id", "2");
		        description.setAttribute("xlink:href",store.getURL()+"/api/languages/"+2);

		        Element meta_description = (Element) schema.getElementsByTagName("meta_description").item(0).getFirstChild();
		        meta_description.appendChild(schema.createCDATASection(product.getDescription()));
		        meta_description.setAttribute("id", "2");
		        meta_description.setAttribute("xlink:href",store.getURL()+"/api/languages/"+2);   
	

	        }
	    
	        if (product.getHelp()!=null){
		        Element description_short = (Element) schema.getElementsByTagName("description").item(0).getFirstChild();
		        description_short.appendChild(schema.createCDATASection(product.getHelp()));
		        description_short.setAttribute("id", "2");
		        description_short.setAttribute("xlink:href",store.getURL()+"/api/languages/"+2);  
	        }
	        
	        Element meta_keywords = (Element) schema.getElementsByTagName("meta_keywords").item(0).getFirstChild();
	        meta_keywords.appendChild(schema.createCDATASection("name"));
	        meta_keywords.setAttribute("id", "2");
	        meta_keywords.setAttribute("xlink:href",store.getURL()+"/api/languages/"+2);    
	        
//		        Element available_now = (Element) schema.getElementsByTagName("available_now").item(0).getFirstChild();
//		        available_now.appendChild(schema.createCDATASection("name"));
//		        available_now.setAttribute("id", "1");
//		        available_now.setAttribute("xlink:href",store.getURL()+"/api/languages/"+1);   
//		        
//		        Element available_later = (Element) schema.getElementsByTagName("available_later").item(0).getFirstChild();
//		        available_later.appendChild(schema.createCDATASection("available later"));
//		        available_later.setAttribute("id", "1");
//		        available_later.setAttribute("xlink:href",store.getURL()+"/api/languages/"+1);   
//			

	        HashMap<String,Object> productOpt = new HashMap();
	        productOpt.put("resource", "products");
	       
	        
			boolean doPut=false;
			
					
				if (!mapProducts.isEmpty() && mapProducts.get(product.get_ID())!=null)
					doPut=true;
				
				long id = 0;
				
				if (doPut){
			         schema.getElementsByTagName("id").item(0).setTextContent(Integer.toString(mapProducts.get(product.get_ID())));
					 productOpt.put("id", mapProducts.get(product.get_ID()));
					 productOpt.put("putXml", ws.DocumentToString(schema));
			        Document ws_product = ws.edit(productOpt);     
			        System.out.println(ws.DocumentToString(ws_product));
			        if (ws_product.getElementsByTagName("stock_available").getLength()>0){
				        Node storage= ws_product.getElementsByTagName("stock_available").item(0);
				        int storage_id=Integer.valueOf(storage.getFirstChild().getNextSibling().getFirstChild().getNodeValue());
				        updateStorageMap(product, storage_id);
			        }
			        id = mapProducts.get(product.get_ID());
			        
				}
				else
				{
					try {
					productOpt.put("postXml", ws.DocumentToString(schema));
			        Document ws_product = ws.add(productOpt);     
			        System.out.println(ws.DocumentToString(ws_product));
			        id = Integer.valueOf(ws.getCharacterDataFromElement((Element) ws_product.getElementsByTagName("id").item(0)));
			        if (ws_product.getElementsByTagName("stock_available").getLength()>0){
				        Node storage= ws_product.getElementsByTagName("stock_available").item(0);
				        int storage_id=Integer.valueOf(storage.getFirstChild().getNextSibling().getFirstChild().getNodeValue());
				        updateStorageMap(product, storage_id);
			        }    
					} 
					catch (Exception e) {
						// TODO: handle exception
						System.out.println("Product added "+product.getValue()+" error uploading ");
					}
				}
				
				//ws.getResponseContent();
				
				
				
				System.out.printf("Product added "+product.getValue()+" ID: %d\n", id);
					
				if (!doPut){
				// add to mapping table
					X_MZI_EShop_Mapping mapping= new X_MZI_EShop_Mapping(Env.getCtx(),0,null);
					mapping.setAD_Table_ID(MProduct.Table_ID);
					mapping.setEshop_Ref_ID(toIntExact(id) );
					mapping.setRecord_ID(product.get_ID());
					mapping.setExternalSystem(X_MZI_EShop_Mapping.EXTERNALSYSTEM_PrestaShop);
					mapping.setW_Store_ID(store.get_ID());
					mapping.saveEx();
					mapProducts.put(product.get_ID(), toIntExact(id));
				}
				
				
			} catch (PrestaShopWebserviceException | TransformerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return e.getMessage();
			}
			
		
		}
		
		long end = Calendar.getInstance().getTimeInMillis();
		System.out.printf("Products - (POST) - Execution time: %.2f seconds\n" , (end - start)/1000.0);
		
		MMZI_ES_Synchronizations synchsucces=new MMZI_ES_Synchronizations(Env.getCtx(), 0, null);
		synchsucces.setAD_Org_ID(store.getAD_Org_ID());
		synchsucces.setSuccess(true);
		synchsucces.setExternalSystem("PS");
		synchsucces.setTimestampLocal(new Timestamp(start));
		synchsucces.setSynchronizationType(MMZI_ES_Synchronizations.SYNCHRONIZATIONTYPE_Product);
		synchsucces.setW_Store_ID(store.get_ID());
		synchsucces.saveEx();
		setMapProducts(mapProducts);
		
		if (errorproducts.equals("")) return "OK";
		
		return errorproducts;
	}
	
	private List<MClassification> getCategoriesByProduct(int m_product_id , int  C_Vocabulary_ID) {

		
		List<MClassification> categories=new Query(Env.getCtx(), MClassification.Table_Name, " c_classification_id in (select ci.c_classification_id from c_classificationinstance ci "
				+ " join  c_classification cl on cl.c_classification_id=ci.c_classification_id where ci.record_id=? AND  cl.C_Vocabulary_ID=?) ", null)
				.setParameters(m_product_id, C_Vocabulary_ID)
				.setClient_ID()
				.setOrderBy(MClassification.COLUMNNAME_C_Classification_ID)
				.list();
		
		List<MClassification> final_categories= new ArrayList<MClassification>(categories);
		for (MClassification cat:categories){
			MClassification category = new MClassification ( Env.getCtx() ,cat.getC_ParentClassification_ID(),null);
			if (final_categories.contains(category))
				continue;
			addParentCat(final_categories, category );
		}
		return final_categories;
	}	
	
	private void addParentCat(List<MClassification> final_categories, MClassification cat ){
		if (cat.get_ID()>0)
			final_categories.add(cat);
		MClassification category = new MClassification ( Env.getCtx() ,cat.getC_ParentClassification_ID(),null);
		if (final_categories.contains(category))
			return;
		if (cat.getC_ParentClassification_ID()==0)
			return;
		
		addParentCat(final_categories, category );
	}
	
	public void initialize() {
			long start = Calendar.getInstance().getTimeInMillis();
			
			try {
				List<String> fields = Collections.emptyList();
				PrestaShopMapperResponse<Products> result = this.getMapper().list(Products.class, fields, this.getFilters(), Sort.EMPTY_SORT, Limit.EMPTY_LIMIT);
				Products resource = result.getResource();
				for (int i=0;i<resource.size();i++){
					Product rst = resource.get(i);
					PrestaShopPojoEntity entity=(PrestaShopPojoEntity) rst;
					this.getMapper().delete(entity);
					System.out.println("Product deleted ID: " + rst.getId());
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			DB.executeUpdate("DELETE FROM MZI_EShop_Mapping WHERE AD_Table_ID="+MProduct.Table_ID+" AND W_Store_ID="+store.get_ID(),null);
			DB.executeUpdate("DELETE FROM MZI_ES_Synchronizations WHERE SYNCHRONIZATIONTYPE='"+ MMZI_ES_Synchronizations.SYNCHRONIZATIONTYPE_Product+"' AND W_Store_ID="+store.get_ID(),null);
			DB.executeUpdate("DELETE FROM MZI_ES_Synchronizations WHERE SYNCHRONIZATIONTYPE='"+ MMZI_ES_Synchronizations.SYNCHRONIZATIONTYPE_StockCurrent+"' AND W_Store_ID="+store.get_ID(),null);
			DB.executeUpdate("DELETE FROM MZI_EShop_Mapping WHERE AD_Table_ID="+MStorageOnHand.Table_ID+" AND W_Store_ID="+store.get_ID(),null);
			long end = Calendar.getInstance().getTimeInMillis();
			System.out.printf("Products - testGetOnlyID - Execution time: %.5f seconds\n", (end - start)/1000.0);
	}

	private List<MProduct> getProductsToSynch( MMZI_ES_Synchronizations synch,int  C_Vocabulary_ID) {
		
		if ( synch==null)
			lastsuccess= new Timestamp(0);
		else 
			lastsuccess=synch.getTimestampLocal();	
		
		List<MProduct> products=new Query(Env.getCtx(), MProduct.Table_Name, " ( Updated>? "
				+ " or (SELECT max(pp.Updated) FROM m_productprice pp join m_pricelist_version pv on pp.m_pricelist_version_id=pp.m_pricelist_version_id"
				+ " WHERE pv.m_pricelist_id=? AND m_product.m_product_id=pp.m_product_id  )>?"
				+ " or (SELECT max(ci.Updated) FROM C_ClassificationInstance ci join c_classification cl on cl.c_classification_id=ci.c_classification_id " + 
				"	 where cl.C_Vocabulary_ID=? AND ci.record_id=M_Product_ID )>? )"
				+ " AND M_Product_ID in ( select ci.record_id from  C_ClassificationInstance ci join c_classification cl on cl.c_classification_id=ci.c_classification_id " 
				+ " where cl.C_Vocabulary_ID=?) ", null)
				.setParameters(lastsuccess,store.getM_PriceList_ID(),lastsuccess,C_Vocabulary_ID,lastsuccess, C_Vocabulary_ID)
				.setClient_ID()
				.list();
	
		return products;
	}

	private void updateStorageMap(MProduct product, int storage_id){
		 if (!mapStorages.containsKey(product.get_ID())&& storage_id>0 ){
				X_MZI_EShop_Mapping mapping= new X_MZI_EShop_Mapping(Env.getCtx(),0,null);
				mapping.setAD_Table_ID(MStorageOnHand.Table_ID);
				mapping.setEshop_Ref_ID(storage_id );
				mapping.setRecord_ID(product.get_ID());
				mapping.setExternalSystem(X_MZI_EShop_Mapping.EXTERNALSYSTEM_PrestaShop);
				mapping.setW_Store_ID( store.get_ID());
				mapping.saveEx();
				mapStorages.put(product.get_ID(), storage_id);
			 
		 }
	 
   }
	private PSWebServiceClient ws;	
	
	


	
}
