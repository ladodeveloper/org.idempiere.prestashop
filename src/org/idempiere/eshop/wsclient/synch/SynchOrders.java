/**
 * 
 */
package org.idempiere.eshop.wsclient.synch;


import static java.lang.Math.toIntExact;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.compiere.util.Env;
import org.compiere.util.Trx;
import org.idempiere.eshop.model.X_MZI_EShop_Mapping;
import org.compiere.model.MMZI_ES_Synchronizations;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.MProduct;
import org.compiere.model.MRequest;
import org.compiere.model.MStore;
import org.compiere.model.MUser;
import org.compiere.model.MBPartner;
import org.compiere.model.MBPartnerLocation;
import org.compiere.model.MLocation;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;

/**
 * @author Edgar Gonzalez
 *
 */
public class SynchOrders extends EShopInitialization {

	private MStore store=null;
	private Map<Integer ,Integer > mapProducts = new HashMap<Integer ,Integer >();
	private Map<Integer ,Integer > mapOrders = new HashMap<Integer ,Integer >();
	private Map<Integer ,Integer > mapLocations = new HashMap<Integer ,Integer >();
	private Map<Integer ,Integer > mapCustomers = new HashMap<Integer ,Integer >();
	
	private Timestamp lastsuccess;
	private PSWebServiceClient ws;	
	private MOrder m_order=null;
	private MBPartner m_bpartner=null;
	private MBPartnerLocation m_bp_location=null;
	
	private static final int IMG_WIDTH = 1280;
	private static final int IMG_HEIGHT = 960;

	protected Trx trx;
	protected String trxName;
	/**
	 * 
	 */
	public SynchOrders(MStore store) {
		
		super(store);
		this.store=store;

		if (this.ws==null)
		    this.ws = new PSWebServiceClient(store.getURL(),store.getWStoreUserPW(),false);

	}
	
	public void run() {
		
		trxName = Trx.createTrxName();
		trx = Trx.get(trxName, true);
		
		MMZI_ES_Synchronizations synch = MMZI_ES_Synchronizations.getLastSuccess(Env.getCtx(),
				MMZI_ES_Synchronizations.SYNCHRONIZATIONTYPE_Order, MMZI_ES_Synchronizations.EXTERNALSYSTEM_PrestaShop , store.getW_Store_ID(), 0, trxName);
		
		long start = Calendar.getInstance().getTimeInMillis();
		
		mapProducts=getMapProducts();
		mapCustomers=getMapCustomers(true);
		mapOrders=getMapOrders(true);
		mapLocations=getMapLocations(true);
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd%20HH:mm:ss");
		String datefilter  = (synch==null?"2018-06-01%2012:00:00": dateFormat.format(synch.getTimestampLocal()));
	
		try {
			HashMap<String,Object> orders_to_synch = new HashMap();
			orders_to_synch.put("resource", "orders");
			orders_to_synch.put("filter", "[date_upd]=%3E["+datefilter+"]&date=1");
			orders_to_synch.put("date", "1");
			Document ws_orders = ws.get(orders_to_synch);
			Element docEle = ws_orders.getDocumentElement();
			NodeList orders = docEle.getElementsByTagName("order");
			System.out.println("No. of orders to import from PrestaShop : " + orders.getLength());

			if (orders != null && orders.getLength() > 0) {
				for (int i = 0; i < orders.getLength(); i++) {
					Node node = orders.item(i);
					importOrder(node.getAttributes().getNamedItem("xlink:href").getNodeValue(),Integer.parseInt(node.getAttributes().getNamedItem("id").getNodeValue()));
				}
			} 
			
			long end = Calendar.getInstance().getTimeInMillis();
			System.out.printf("Orders synch - (POST) - Execution time: %.2f seconds\n" , (end - start)/1000.0);
			
			MMZI_ES_Synchronizations synchsucces=new MMZI_ES_Synchronizations(Env.getCtx(), 0, trxName);
			synchsucces.setAD_Org_ID(store.getAD_Org_ID());
			synchsucces.setSuccess(true);
			synchsucces.setExternalSystem("PS");
			synchsucces.setTimestampLocal(new Timestamp(start));
			synchsucces.setSynchronizationType(MMZI_ES_Synchronizations.SYNCHRONIZATIONTYPE_Order);
			synchsucces.setW_Store_ID(store.get_ID());
			synchsucces.saveEx();

		} catch (PrestaShopWebserviceException  e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			trx.rollback();
			return;
		} 
	
		trx.commit();
		
	}
	
	private boolean importOrder(String url, int ps_order_id) throws PrestaShopWebserviceException {

		HashMap<String,Object> order = new HashMap();
		order.put("url", url);
		Document ws_order = ws.get(order);
		NodeList lorder = ws_order.getElementsByTagName("order");
		Node nNode = lorder.item(0);
		Element eElement = (Element) nNode;

		
		int c_order_id=getKeyByValue(mapOrders, ps_order_id);
		
		if (eElement.getElementsByTagName("valid").item(0).getTextContent().equals("0")) {
			if (c_order_id==0) return true;
			else 
			if (c_order_id>0) {
				
				m_order=new MOrder(Env.getCtx(),c_order_id,trxName);
				if (m_order.getDocStatus().equals(MOrder.DOCSTATUS_Drafted)) {
					m_order.processIt(MOrder.DOCACTION_Void);
				}
				
				MRequest request=new MRequest(Env.getCtx(), m_order.getSalesRep_ID(),1000008,"Zrusenie objednavky : "+ m_order.getC_BPartner().getName()+ "; "+m_order.getDateOrdered()+"; "+m_order.getGrandTotal() , false, trxName);
				request.setAD_Table_ID(MOrder.Table_ID);
				request.setRecord_ID(m_order.get_ID());
				request.setC_BPartner_ID(m_order.getC_BPartner_ID());
				request.setC_Order_ID(m_order.getC_Order_ID());
				request.saveEx();
				
				
			}
				
		}
			
		m_order=new MOrder(Env.getCtx(),c_order_id,trxName);
		if (c_order_id>0 && m_order.getDocStatus().equals(MOrder.DOCSTATUS_Completed) ) {
			//TODO: what update I can do on Completed order (delivery date, cancelation... ?  
			return true;
		}
		m_order.setDocumentNo( store.getName()+"_"+(eElement.getElementsByTagName("reference").item(0).getTextContent()));
		
		m_order.setDateOrdered(getTimestamp(eElement.getElementsByTagName("date_add").item(0).getTextContent()));
		m_order.setClientOrg(store.getAD_Client_ID(), store.getAD_Org_ID());
		processCustomer(eElement.getElementsByTagName("id_customer").item(0).getAttributes().getNamedItem("xlink:href").getNodeValue());
		
		//getNodeValue().equals(anObject)
		processAddress(eElement.getElementsByTagName("id_address_delivery").item(0).getAttributes().getNamedItem("xlink:href").getNodeValue(),true,false);
		processAddress(eElement.getElementsByTagName("id_address_invoice").item(0).getAttributes().getNamedItem("xlink:href").getNodeValue(),false,true);
		m_order.setM_Warehouse_ID(store.getM_Warehouse_ID());
		m_order.setC_DocType_ID(store.getC_DocType_ID());
		m_order.setPOReference( store.getName()+"_"+ps_order_id);
		m_order.setM_PriceList_ID(store.getM_PriceList_ID());
		m_order.setIsSOTrx(true);
		m_order.saveEx();
		
		processOrderLines(ws_order);
		BigDecimal shippingcosts=new BigDecimal(eElement.getElementsByTagName("total_shipping").item(0).getTextContent());
		if (shippingcosts.signum()>0) {
			MOrderLine m_line= new MOrderLine(m_order );
			m_line.setM_Product_ID(1020378);//TODO
			m_line.setQty(Env.ONE);
			m_line.setPrice(shippingcosts);
			m_line.saveEx();
		}
		if (c_order_id==0)
			addMapping(mapOrders, store.get_ID(), MOrder.Table_ID, ps_order_id, m_order.get_ID(), trxName);
		
		MRequest request=new MRequest(Env.getCtx(), m_order.getSalesRep_ID(),1000008,m_order.getC_BPartner().getName()+ "; "+m_order.getDateOrdered()+"; "+m_order.getGrandTotal() , false, trxName);
		request.setAD_Table_ID(MOrder.Table_ID);
		request.setRecord_ID(m_order.get_ID());
		request.setC_BPartner_ID(m_order.getC_BPartner_ID());
		request.setC_Order_ID(m_order.getC_Order_ID());
		request.saveEx();
		
		return true;
	}
	
	private Timestamp getTimestamp(String timestamp) {
		return  Timestamp.valueOf(timestamp);
	}

	
	private boolean processOrderLines (Document ws_order) throws PrestaShopWebserviceException {
		NodeList orderlines = ws_order.getElementsByTagName("order_row");
		if (orderlines != null && orderlines.getLength() > 0) {
			if(m_order.getLines().length>0) {
				for(MOrderLine line:m_order.getLines()) {
					line.deleteEx(true);
				}
			}
			
			for (int i = 0; i < orderlines.getLength(); i++) {
				Node nodeline = orderlines.item(i); 
				Element line = (Element) nodeline;
				int ps_product_id=Integer.parseInt(line.getElementsByTagName("product_id").item(0).getTextContent());
				BigDecimal ps_qty=new BigDecimal(line.getElementsByTagName("product_quantity").item(0).getTextContent());
				BigDecimal price=Env.ZERO;
				if (store.getM_PriceList().isTaxIncluded())
						price=new BigDecimal(line.getElementsByTagName("unit_price_tax_incl").item(0).getTextContent());
					else
						price=new BigDecimal(line.getElementsByTagName("unit_price_tax_excl").item(0).getTextContent());
				
				int m_product_id=getKeyByValue(mapProducts, ps_product_id);
				if (m_product_id>0) {
					MOrderLine m_line= new MOrderLine(m_order );
					m_line.setM_Product_ID(m_product_id);
					m_line.setQty(ps_qty);
					m_line.setPrice(price);
					m_line.saveEx();
				}
			}
			
		} 
		return true;
	}
	private boolean processCustomer (String url) throws PrestaShopWebserviceException {
		HashMap<String,Object> customer = new HashMap();
		customer.put("url", url);
		Document ws_customer = ws.get(customer);
		NodeList lcustomer = ws_customer.getElementsByTagName("customer");
		Node nNode = lcustomer.item(0);
		Element eElement = (Element) nNode;
		
		int ps_bpartner_id=Integer.parseInt(eElement.getElementsByTagName("id").item(0).getTextContent());
		int c_bpartner_id=getKeyByValue(mapCustomers, ps_bpartner_id);
		m_bpartner=new MBPartner(Env.getCtx(),c_bpartner_id,trxName);

		if (c_bpartner_id==0) {
			String firstname= eElement.getElementsByTagName("firstname").item(0).getTextContent();
			String lastname= eElement.getElementsByTagName("lastname").item(0).getTextContent();
			m_bpartner.setName(firstname+" "+lastname);
			m_bpartner.setName2(store.getName()+"_"+ps_bpartner_id);
			m_bpartner.setC_BP_Group_ID(store.get_ValueAsInt("C_BP_Group_ID"));
			m_bpartner.setC_TaxGroup_ID(store.get_ValueAsInt("C_TaxGroup_ID"));
			m_bpartner.setIsCustomer(true);
		}
		m_bpartner.saveEx();
		MUser user=null;
		MUser[] userlist=MUser.getOfBPartner(Env.getCtx(), m_bpartner.getC_BPartner_ID(),trxName);
		if (userlist.length>0)
			user=userlist[0];
		
		if (user==null) {
			user=new MUser(Env.getCtx(),0,trxName);
			user.setValue(m_bpartner.getValue());
			user.setName(m_bpartner.getName());
			user.setEMail(eElement.getElementsByTagName("email").item(0).getTextContent());
			user.setC_BPartner_ID(m_bpartner.getC_BPartner_ID());
			user.saveEx();
		}
		m_order.setC_BPartner_ID(m_bpartner.get_ID());
		if (c_bpartner_id==0)
			addMapping(mapCustomers, store.get_ID(), MBPartner.Table_ID, ps_bpartner_id, m_bpartner.get_ID(), trxName);
		
		return true;	
	}
	
	private boolean processAddress (String url, boolean IsShipTo, boolean IsBillTo) throws PrestaShopWebserviceException {
		HashMap<String,Object> address = new HashMap();
		address.put("url", url);
		Document ws_address = ws.get(address);
		NodeList laddress = ws_address.getElementsByTagName("address");
		Node nNode = laddress.item(0);
		Element eElement = (Element) nNode;

		int ps_bp_location_id=Integer.parseInt(eElement.getElementsByTagName("id").item(0).getTextContent());
		int c_bp_location_id=getKeyByValue(mapLocations, ps_bp_location_id);
		
		if (c_bp_location_id==0) {
			m_bp_location=new MBPartnerLocation(Env.getCtx(),c_bp_location_id,trxName);
			if (m_bp_location.getC_Location_ID()==0) {
				MLocation location= new MLocation (Env.getCtx(),0,trxName);
				location.setAddress1(eElement.getElementsByTagName("address1").item(0).getTextContent());
				location.setAddress2(eElement.getElementsByTagName("address2").item(0).getTextContent());
				location.setCity(eElement.getElementsByTagName("city").item(0).getTextContent());
				location.setPostal(eElement.getElementsByTagName("postcode").item(0).getTextContent());
				location.saveEx();
				m_bp_location.setC_Location_ID(location.getC_Location_ID());
			}
			
			m_bp_location.setC_BPartner_ID(m_bpartner.getC_BPartner_ID());
			m_bp_location.setIsShipTo(IsShipTo);
			m_bp_location.setIsBillTo(IsBillTo);
			String Name=(eElement.getElementsByTagName("company").item(0).getTextContent()!=null?eElement.getElementsByTagName("company").item(0).getTextContent()+" - ":"")+
					    (eElement.getElementsByTagName("firstname").item(0).getTextContent()!=null?eElement.getElementsByTagName("firstname").item(0).getTextContent()+" ":"")+
					    (eElement.getElementsByTagName("lastname").item(0).getTextContent()!=null?eElement.getElementsByTagName("lastname").item(0).getTextContent()+" ":"");
			m_bp_location.setName(Name );
			m_bp_location.setPhone(eElement.getElementsByTagName("phone").item(0).getTextContent());
			m_bp_location.setPhone2(eElement.getElementsByTagName("phone_mobile").item(0).getTextContent());
			m_bp_location.setIsPayFrom(IsBillTo);
			m_bp_location.setIsRemitTo(IsBillTo);
			m_bp_location.saveEx();
		}else
			m_bp_location=new MBPartnerLocation(Env.getCtx(),c_bp_location_id,trxName);
		
		if (IsShipTo) m_order.setC_BPartner_Location_ID(m_bp_location.getC_BPartner_Location_ID());
		if (IsBillTo) m_order.setBill_Location_ID(m_bp_location.getC_BPartner_Location_ID());
		if (c_bp_location_id==0)
			addMapping(mapLocations, store.get_ID(), MLocation.Table_ID, ps_bp_location_id, m_bp_location.get_ID(), trxName);
		
		return true;
	}
		
}

	