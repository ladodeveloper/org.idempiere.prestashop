/**
 * 
 */
package org.idempiere.eshop.wsclient.synch;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.lang.Math.toIntExact;

import org.adempiere.model.ProductPriceValidator;
import org.compiere.model.MClassification;
import org.compiere.model.MImage;
import org.compiere.model.MMZI_ES_Synchronizations;
import org.compiere.model.MProduct;
import org.compiere.model.MProductCategory;
import org.compiere.model.MProductPrice;
import org.compiere.model.MProductPricing;
import org.compiere.model.MStore;
import org.compiere.model.Query;
import org.compiere.process.M_Production_Run;
import org.compiere.util.CPreparedStatement;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.idempiere.eshop.model.X_MZI_EShop_Mapping;


import com.edgaragg.pshop4j.model.Limit;
import com.edgaragg.pshop4j.model.Sort;
import com.edgaragg.pshop4j.modeling.PrestaShopMapperResponse;
import com.edgaragg.pshop4j.modeling.enums.PShopBoolean;
import com.edgaragg.pshop4j.pojos.PrestaShopPojoEntity;
import com.edgaragg.pshop4j.pojos.entities.Category;
import com.edgaragg.pshop4j.pojos.entities.LanguageElement;
import com.edgaragg.pshop4j.pojos.entities.Product;
import com.edgaragg.pshop4j.pojos.list.Categories;
import com.edgaragg.pshop4j.pojos.list.LanguageElements;
import com.edgaragg.pshop4j.pojos.list.Products;

import org.apache.commons.lang.StringEscapeUtils;
/**
 * @author Edgar Gonzalez
 *
 */
public class SynchProducts extends EShopInitialization {

	private MStore store=null;
	private Map<Integer ,Integer > mapProducts = new HashMap<Integer ,Integer >();
	private Timestamp lastsuccess;
	
	/**
	 * 
	 */
	public SynchProducts(MStore store) {
		
		super(store);
		
		this.store=store;
	}
	
	public void run() {
		MMZI_ES_Synchronizations synch = MMZI_ES_Synchronizations.getLastSuccess(Env.getCtx(),
				MMZI_ES_Synchronizations.SYNCHRONIZATIONTYPE_Product, MMZI_ES_Synchronizations.EXTERNALSYSTEM_PrestaShop , store.getW_Store_ID(), 0, null);

		
		mapProducts=getMapProducts();
		
		long start = Calendar.getInstance().getTimeInMillis();
		
		List<MProduct> products=getProductsToSynch( synch ,store.getC_Vocabulary_ID());
		
		
		
		for (MProduct product:products){
			System.out.println("Product started "+product.getValue());
			Product entity = new Product();

			Map<String,Object> opt=new HashMap();
			opt.put("resouce","Product");
		    
			if (product.isActive())
				entity.setActive(PShopBoolean.TRUE);
			else 
				entity.setActive(PShopBoolean.FALSE);
			
			
			MProductPrice pp=MProductPrice.getLatest(Env.getCtx(),store.getM_PriceList_ID(), product.getM_Product_ID(), null);
			
			entity.setPrice(pp.getPriceList());
			entity.setAvailableForOrder(PShopBoolean.TRUE);
			entity.setIdTaxRulesGroup(1);
			entity.setMinimalQuantity(1);
			entity.setShowPrice(PShopBoolean.TRUE);
			entity.setIdCategoryDefault(getCategorieDefault(product,store));//
			
			entity.setDescription(new LanguageElements());
			entity.getDescription().add(new LanguageElement().withId(1).withContent(StringEscapeUtils.escapeXml(product.getDescription())));
			entity.getDescription().add(new LanguageElement().withId(2).withContent(StringEscapeUtils.escapeXml(product.getDescription())));
			entity.setLinkRewrite(new LanguageElements());
			entity.getLinkRewrite().add(new LanguageElement().withId(1).withContent(StringEscapeUtils.escapeXml(product.getValue())));
			entity.getLinkRewrite().add(new LanguageElement().withId(2).withContent(StringEscapeUtils.escapeXml(product.getValue())));
			entity.setName(new LanguageElements());
			entity.getName().add(new LanguageElement().withId(1).withContent(StringEscapeUtils.escapeXml(product.getName())));
			entity.getName().add(new LanguageElement().withId(2).withContent(StringEscapeUtils.escapeXml(product.getName())));

			PrestaShopMapperResponse<Product> result = null;
			boolean doPut=false;
			if (!mapProducts.isEmpty() && mapProducts.get(product.get_ID())!=null)
				doPut=true;
			
			if (doPut){
				entity.setId(mapProducts.get(product.get_ID()));
				result = this.getMapper().put(entity);
			}
			else 
				 result = this.getMapper().post(entity);

			
			Product resource = result.getResource();
			long id = resource.getId();
			
			System.out.printf("Product added "+product.getValue()+" ID: %d\n", id);
				
			if (!doPut){
				// add to mapping table
				addMapping(mapProducts, store.get_ID(), MProduct.Table_ID, toIntExact(id), product.get_ID(), null);
			}
			
		}
		
		long end = Calendar.getInstance().getTimeInMillis();
		System.out.printf("Products - (POST) - Execution time: %.2f seconds\n" , (end - start)/1000.0);
		
		MMZI_ES_Synchronizations synchsucces=new MMZI_ES_Synchronizations(Env.getCtx(), 0, null);
		synchsucces.setAD_Org_ID(store.getAD_Org_ID());
		synchsucces.setSuccess(true);
		synchsucces.setExternalSystem("PS");
		synchsucces.setTimestampLocal(new Timestamp(start));
		synchsucces.setSynchronizationType(MMZI_ES_Synchronizations.SYNCHRONIZATIONTYPE_Product);
		synchsucces.setW_Store_ID(store.get_ID());
		synchsucces.saveEx();
		setMapProducts(mapProducts);

	}
	
	public void initialize() {
			long start = Calendar.getInstance().getTimeInMillis();
			
			try {
				List<String> fields = Collections.emptyList();
				PrestaShopMapperResponse<Products> result = this.getMapper().list(Products.class, fields, this.getFilters(), Sort.EMPTY_SORT, Limit.EMPTY_LIMIT);
				Products resource = result.getResource();
				for (int i=0;i<resource.size();i++){
					Product rst = resource.get(i);
					PrestaShopPojoEntity entity=(PrestaShopPojoEntity) rst;
					this.getMapper().delete(entity);
					System.out.println("Prdocut deleted ID: " + rst.getId());
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			DB.executeUpdate("DELETE FROM MZI_EShop_Mapping WHERE AD_Table_ID="+MProduct.Table_ID+" AND W_Store_ID="+store.get_ID(),null);
			long end = Calendar.getInstance().getTimeInMillis();
			System.out.printf("Products - testGetOnlyID - Execution time: %.5f seconds\n", (end - start)/1000.0);
	}

	private List<MProduct> getProductsToSynch( MMZI_ES_Synchronizations synch,int  C_Vocabulary_ID) {
		
		if ( synch==null)
			lastsuccess= new Timestamp(0);
		else 
			lastsuccess=synch.getTimestampLocal();	
		
		List<MProduct> products=new Query(Env.getCtx(), MProduct.Table_Name, " ( Updated>? "
				+ " or (SELECT max(pp.Updated) FROM m_productprice pp join m_pricelist_version pv on pp.m_pricelist_version_id=pp.m_pricelist_version_id"
				+ " WHERE pv.m_pricelist_id=? AND m_product.m_product_id=pp.m_product_id  )>?) "
				+ " AND M_Product_ID in (select ci.record_id from  C_ClassificationInstance ci where ci.C_Vocabulary_ID=?) ", null)
				.setParameters(lastsuccess,store.getM_PriceList_ID(),lastsuccess, C_Vocabulary_ID)
				.setClient_ID()
				.list();
	
		return products;
	}
	
	private PSWebServiceClient ws;	
	
	


	
}
