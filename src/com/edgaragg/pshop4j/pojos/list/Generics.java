/**
 * 
 */
package com.edgaragg.pshop4j.pojos.list;

import java.util.ArrayList;
import java.util.List;

import com.edgaragg.pshop4j.model.Resources;
import com.edgaragg.pshop4j.modeling.annotations.PrestaShopElement;
import com.edgaragg.pshop4j.modeling.annotations.PrestaShopList;
import com.edgaragg.pshop4j.modeling.annotations.PrestaShopResource;
import com.edgaragg.pshop4j.pojos.PrestaShopPojoList;
import com.edgaragg.pshop4j.pojos.entities.Generic;

/**
 * @author Edgar Gonzalez
 *
 */
@PrestaShopResource(Resources.generic)
@PrestaShopElement("generic")
public class Generics extends PrestaShopPojoList<Generic> {
	
	@PrestaShopElement("generic")
	@PrestaShopList(Generic.class)
	private List<Generic> objects;
	
	public Generics() {
		super();
	}
	
	@Override
	protected List<Generic> createInnerList() {
		this.objects = new ArrayList<Generic>();
		return this.objects;
	}

}
